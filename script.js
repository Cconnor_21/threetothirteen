// Three to Thirteen

// Computer player ai actions

//52 cards in a deck and 2 jokers


var hands = {
  0:{
    "hand": "first hand",
    "numOfCards": 3,
    "wildCard": 3
  },
  1:{
    "hand": "second hand",
    "numOfCards": 4,
    "wildCard": 4
  },
  2:{
    "hand": "third hand",
    "numOfCards": 5,
    "wildCard": 5
  },
  3:{
    "hand": "fourth hand",
    "numOfCards": 6,
    "wildCard": 6
  },
  4:{
    "hand": "fifth hand",
    "numOfCards": 7,
    "wildCard": 7
  },
  5:{
    "hand": "sixth hand",
    "numOfCards": 8,
    "wildCard": 8
  },
  6:{
    "hand": "seventh hand",
    "numOfCards": 9,
    "wildCard": 9
  },
  7:{
    "hand": "eighth hand",
    "numOfCards": 10,
    "wildCard": 10
  },
  8:{
    "hand": "ninth hand",
    "numOfCards": 11,
    "wildCard": "jack"
  },
  9:{
    "hand": "tenth hand",
    "numOfCards": 12,
    "wildCard": "queen"
  },
  10:{
    "hand": "eleventh hand",
    "numOfCards": 13,
    "wildCard": "king"
  },
}

var cards = {
  0:{
    "value": "Ace",
    "numValue": 1,
    "loseValue": 15,
    "suit": "hearts"
  },
  1:{
    "value": "Ace",
    "numValue": 1,
    "loseValue": 15,
    "suit": "diamonds"
  },
  2:{
    "value": "Ace",
    "numValue": 1,
    "loseValue": 15,
    "suit": "spades"
  },
  3:{
    "value": "Ace",
    "numValue": 1,
    "loseValue": 15,
    "suit": "clubs"
  },

  4:{
    "value": "two",
    "numValue": 2,
    "loseValue": 2,
    "suit": "hearts"
  },
  5:{
    "value": "two",
    "numValue": 2,
    "loseValue": 2,
    "suit": "diamonds"
  },
  6:{
    "value": "two",
    "numValue": 2,
    "loseValue": 2,
    "suit": "spades"
  },
  7:{
    "value": "two",
    "numValue": 2,
    "loseValue": 2,
    "suit": "clubs"
  },
  8:{
    "value": "three",
    "numValue": 3,
    "loseValue": 3,
    "suit": "hearts"
  },
  9:{
    "value": "three",
    "numValue": 3,
    "loseValue": 3,
    "suit": "diamonds"
  },
  10:{
    "value": "three",
    "numValue": 3,
    "loseValue": 3,
    "suit": "spades"
  },
  11:{
    "value": "three",
    "numValue": 3,
    "loseValue": 3,
    "suit": "clubs"
  },
  12:{
    "value": "four",
    "numValue": 4,
    "loseValue": 4,
    "suit": "hearts"
  },
  13:{
    "value": "four",
    "numValue": 4,
    "loseValue": 4,
    "suit": "diamonds"
  },
  14:{
    "value": "four",
    "numValue": 4,
    "loseValue": 4,
    "suit": "spades"
  },
  15:{
    "value": "four",
    "numValue":4,
    "loseValue": 4,
    "suit": "clubs"
  },
  16:{
    "value": "five",
    "numValue": 5,
    "loseValue": 5,
    "suit": "hearts"
  },
  17:{
    "value": "five",
    "numValue": 5,
    "loseValue": 5,
    "suit": "diamonds"
  },
  18:{
    "value": "five",
    "numValue": 5,
    "loseValue": 5,
    "suit": "spades"
  },
  19:{
    "value": "five",
    "numValue": 5,
    "loseValue": 5,
    "suit": "clubs"
  },
  20:{
    "value": "sixth",
    "numValue": 6,
    "loseValue": 6,
    "suit": "hearts"
  },
  21:{
    "value": "sixth",
    "numValue": 6,
    "loseValue": 6,
    "suit": "diamonds"
  },
  22:{
    "value": "sixth",
    "numValue": 6,
    "loseValue": 6,
    "suit": "spades"
  },
  23:{
    "value": "sixth",
    "numValue": 6,
    "loseValue": 6,
    "suit": "clubs"
  },
  24:{
    "value": "seven",
    "numValue": 7,
    "loseValue": 7,
    "suit": "hearts"
  },
  25:{
    "value": "seven",
    "numValue": 7,
    "loseValue": 7,
    "suit": "diamonds"
  },
  26:{
    "value": "seven",
    "numValue": 7,
    "loseValue": 7,
    "suit": "spades"
  },
  27:{
    "value": "seven",
    "numValue": 7,
    "loseValue": 7,
    "suit": "clubs"
  },
  28:{
    "value": "eight",
    "numValue":8,
    "loseValue": 8,
    "suit": "hearts"
  },
  29:{
    "value": "eight",
    "numValue":8,
    "loseValue": 8,
    "suit": "diamonds"
  },
  30:{
    "value": "eight",
    "numValue": 8,
    "loseValue": 8,
    "suit": "spades"
  },
  31:{
    "value": "eight",
    "numValue": 8,
    "loseValue": 8,
    "suit": "clubs"
  },
  32:{
    "value": "nine",
    "numValue": 9,
    "loseValue": 9,
    "suit": "hearts"
  },
  33:{
    "value": "nine",
    "numValue":9,
    "loseValue": 9,
    "suit": "diamonds"
  },
  34:{
    "value": "nine",
    "numValue": 9,
    "loseValue": 9,
    "suit": "spades"
  },
  35:{
    "value": "nine",
    "numValue": 9,
    "loseValue": 9,
    "suit": "clubs"
  },
  36:{
    "value": "ten",
    "numValue": 10,
    "loseValue": 10,
    "suit": "hearts"
  },
  37:{
    "value": "ten",
    "numValue": 10,
    "loseValue": 10,
    "suit": "diamonds"
  },
  38:{
    "value": "ten",
    "numValue": 10,
    "loseValue": 10,
    "suit": "spades"
  },
  39:{
    "value": "ten",
    "numValue": 10,
    "loseValue": 10,
    "suit": "clubs"
  },
  40:{
    "value": "jack",
    "numValue": 11,
    "loseValue": 10,
    "suit": "hearts"
  },
  41:{
    "value": "jack",
    "numValue": 11,
    "loseValue": 10,
    "suit": "diamonds"
  },
  42:{
    "value": "jack",
    "numValue": 11,
    "loseValue": 10,
    "suit": "spades"
  },
  43:{
    "value": "jack",
    "numValue": 11,
    "loseValue": 10,
    "suit": "clubs"
  },
  44:{
    "value": "queen",
    "numValue": 12,
    "loseValue": 10,
    "suit": "hearts"
  },
  45:{
    "value": "queen",
    "numValue": 12,
    "loseValue": 10,
    "suit": "diamonds"
  },
  46:{
    "value": "queen",
    "numValue": 12,
    "loseValue": 10,
    "suit": "spades"
  },
  47:{
    "value": "queen",
    "numValue": 12,
    "loseValue": 10,
    "suit": "clubs"
  },
  48:{
    "value": "king",
    "numValue": 13,
    "loseValue": 10,
    "suit": "hearts"
  },
  49:{
    "value": "king",
    "numValue": 13,
    "loseValue": 10,
    "suit": "diamonds"
  },
  50:{
    "value": "king",
    "numValue": 13,
    "loseValue": 10,
    "suit": "spades"
  },
  51:{
    "value": "king",
    "numValue": 13,
    "loseValue": 10,
    "suit": "clubs"
  },
  52:{
    "value": "joker",
    "numValue": 0,
    "loseValue": 25,
    "suit": "none"
  },
  53:{
    "value": "joker",
    "numValue": 0,
    "loseValue": 25,
    "suit": "none"
  }
}

var deck = [];
var computerHand;
var userHand;
var stack = [];
var dealer = "computer";
var hand = 0;
var player;
var keepCards = [];
//for testing
var beforeDeck = [];

var computerHandUI = document.getElementById("computerHand");
var playerHandUI = document.getElementById("computerHand");
var stackUI = document.getElementById("stack");
var currentPlayer = document.getElementById("currentPlayer");


function startFirstHand(){
  dealCards();
  player = "player";
  currentPlayer.innerHTML = "<span>"+ player +"</span>";
}

function dealCards(){
  //deal cards to computer
  //deal cards to user
  userHand = [];
  computerHand = [];
  var user;

  if(dealer == "computer")
  {
    user = "user";
    for(i = 0; i < hands[hand]["numOfCards"] * 2; i++){
      if(user == "user")
      {
        userHand.push(deck[i]);
        user = "computer";
      }
      else{
        computerHand.push(deck[i]);
        user = "user";
      }
    }
  }
  else if(dealer == "user")
  {
    user = "computer";
    for(i = 0; i < hands[hand]["numOfCards"] * 2; i++){
      if(user == "user")
      {
        userHand.push(deck[i]);
        user = "computer";
      }
      else{
        computerHand.push(deck[i]);
        user = "user";
      }

    }
  }
  displayHand(userHand, computerHand, false);
  findDeckCardsToRemove(userHand.concat(computerHand));
  addCardToStack(deck[0]);
}


function displayHand(userHand, computerHand, clickable){
  playerHandUI.innerHTML = "";
  for(i = 0; i < userHand.length; i++)
  {
    if(clickable == true)
    {
      playerHandUI.innerHTML += "<div class='handCard' onclick='removeCardFromUserHand(\"" + i + "\")'>"+ userHand[i]["value"] + " of " + userHand[i]["suit"] +"</div>";
    }
    else{
      playerHandUI.innerHTML += "<div>"+ userHand[i]["value"] + " of " + userHand[i]["suit"] +"</div>";
    }
  }
}

//this function takes an array as a paremeter
function findDeckCardsToRemove(values){
  //initialize new deck
  //add all values exept passed in values to new array
var indexes = [];
    for(i = 0; i < values.length; i++){
        for(y = 0; y < deck.length; y++)
        {
          if(values[i]["value"] === deck[y]["value"] && values[i]["suit"] === deck[y]["suit"]){
            indexes.push(y);
          }
        }
    }
    removeCardsFromDeck(indexes);
}

function removeCardsFromDeck(indexes){
  for(i = 0; i < indexes.length; i++)
  {
    deck.splice(i["value"],1);
  }
}

function addCardToStack(val){
  var s = [];
  stack.unshift(val);
  s.push(val);
  findDeckCardsToRemove(s);
  displayStack();
}


function userSelectCardFromDeck(){
  var s = [];
  userHand.push(deck[0]);
  s.push(deck[0]);
  findDeckCardsToRemove(s);
  document.getElementById("deckButton").disabled = true;
  document.getElementById("stackButton").disabled = true;
  displayHand(userHand, computerHand, true);
}

function userSelectCardFromStack(){
  userHand.push(stack[0]);
  document.getElementById("deckButton").disabled = true;
  document.getElementById("stackButton").disabled = true;
  removeCardFromStack();
  displayHand(userHand, computerHand, true);
  displayStack();
}


//this function is for testing
function saveBeforeDeck(){
  for(i = 0; i < deck.length; i++)
  {
    beforeDeck.push(deck[i]);
  }
}

function createDeck(){
  for(i = 0; i < 54; i++)
  {
    deck.push(cards[i]);
  }
  return deck;
}

function shuffleDeck(){
  for(i = 0; i < 300; i++)
  {
    deck = deck.sort(() => Math.floor(Math.random() * deck.length));
  }
  return deck;
}

function removeCardFromStack(value){
  stack.splice(0,1);
}

function removeCardFromUserHand(index){
  addCardToStack(userHand[index]);
  userHand.splice(index,1);
  displayHand(userHand, computerHand, false);
  changeTurn();
}

function removeCardFromComputerHand(index){
  addCardToStack(computerHand[index]);
  computerHand.splice(index,1);
  displayHand(userHand, computerHand, false);
  changeTurn();
}


function displayStack(){
  stackUI.innerHTML = "";
  if(stack.length != 0)
  {
    stackUI.innerHTML = "<p>" + stack[0]["value"] + " of " + stack[0]["suit"] + "</p>";
  }
}


function changeTurn(){
  if(player == "player")
  {
    player = "computer";
    currentPlayer.innerHTML = "<span>"+ player +"</span>";
    makeComputerActions();
  }
  else if(player == "computer")
  {
    player = "player";
    currentPlayer.innerHTML = "<span>"+ player +"</span>";
    document.getElementById("deckButton").disabled = false;
    document.getElementById("stackButton").disabled = false;
    console.log("player");
  }
}


/////////////////   Computer Actions  //////////////////////

function makeComputerActions(){
  drawCardAction();
}

function drawCardAction(){
  if(checkStackAction() > 0){
    console.log("select card from stack");
    var d = "stack";
    computerSelectCardFromStack();
    ridOfCardAction(d);
  }
  else{
    console.log("select card from deck");
    var d = "deck";
    computerSelectCardFromDeck();
    ridOfCardAction(d);
  }
}

function computerSelectCardFromDeck(){
  var s = [];
  computerHand.push(deck[0]);
  s.push(deck[0]);
  findDeckCardsToRemove(s);
  console.log(computerHand);
}

function computerSelectCardFromStack(){
  computerHand.push(stack[0]);
  removeCardFromStack();
  displayStack();
  console.log(computerHand);
}


function ridOfCardAction(d){
  //get computerHand
  //don't get rid of cards that go together
  //try to get rid of largest value card
  if(d == "stack")
  {
    console.log("1");
    var throwAwayCards = [];
    for(i = 0; i < computerHand.length; i++){
      for(y = 0; y < keepCards.length; y++)
      {
        if(computerHand[i]["value"] != keepCards[y]["value"] || computerHand[i]["suit"] != keepCards[y]["suit"]){
          throwAwayCards.push(computerHand[i]);
        }
      }

    }
  console.log(throwAwayCards);
  highCardAction(throwAwayCards);

  }
  else if(d == "deck"){

    console.log("2");
    var valMatchesList = [];
    var valThrowAwayCards = [];

    //loop through the computer hand and create an array of each indexes amount of duplicates
    for(i = 0; i < computerHand.length; i++)
    {
      var valMatches = 0;
      for(y = 0; y < computerHand.length; y++){
        if(computerHand[y]["value"] == computerHand[i]["value"])
        {
          valMatches++;
        }
      }
      valMatchesList.push(valMatches);
    }

    console.log(valMatchesList);

    //add cards that have less than one duplicate to the valThrowAwayCards array
    //the cards with less than two matches do not go with any other cards

    for(i = 0; i < valMatchesList.length; i++)
    {
      if(valMatchesList[i] < 2)
      {
        valThrowAwayCards.push(computerHand[i]);
      }
    }
    console.log(valThrowAwayCards);
    console.log(valThrowAwayCards.length);
    console.log("3");


    //check if there are duplicate suits in the computer hand
    //if there where no matches in the matches array, valThrowAwayCards should still have 4 values
    if(valThrowAwayCards.length == 4)
    {

      //if the length is 4, none of the values of the cards matche
      //But the suits of the cards could still match
      //loop through the computerHand and find how many matches there are for each index put them in the suitMatchesList
      console.log("check if any suits match");
      var suitMatchesList = [];
      var suitThrowAwayCards = [];
      for(i = 0; i < computerHand.length; i++)
      {
        var suitMatches = 0;
        for(y = 0; y < computerHand.length; y++){
          if(computerHand[y]["suit"] == computerHand[i]["suit"])
          {
            suitMatches++;
          }
        }
        suitMatchesList.push(suitMatches);

      }

      console.log(suitMatchesList);


      //just like with the values, check each card in the suitMatchesList
      //if the index has less than 2 matches it can be thrown away and is added to suitThrowAwayCards
      for(i = 0; i < suitMatchesList.length; i++)
      {
        if(suitMatchesList[i] < 2)
        {
          suitThrowAwayCards.push(computerHand[i]);
        }
        else{
          keepCards.push(computerHand[i]);
        }
      }
      console.log(suitThrowAwayCards);
      console.log(suitThrowAwayCards.length);


      //if the suitThrowAwayCards array is full, all cards can be kept so the largest needs to be thrown away
      //if the suitThrowAwayCards array is empty, none of the cards are a match and the largest needs to be thrown away
      if(suitThrowAwayCards.length == 4 || suitThrowAwayCards.length == 0)
      {

        //throw the highest card away
        console.log("throw the highest card away");
        highCardAction(computerHand);
        console.log("computer Hand");
      }
      else if(suitThrowAwayCards.length == 1){
        //if there is only one throw away card then the other three are the same suit
        //make sure the values are in a workable run
        //check if keep cards are a run
        //sort array in order from highest to lowest
        //check the difference of the highest
        var keepCardVals = [];

        for(i = 0; i < keepCards.length; i++)
        {
          keepCardVals.push(parseInt(keepCards[i]["numValue"]));
        }


        var highestVal = Math.max(...keepCardVals);
        var lowestVal = Math.min(...keepCardVals);
        var middleVal;

        //get middle card
        for(i = 0; i < keepCardVals.length; i++){
          if(keepCardVals != highestVal && keepCardVals != lowestVal){
            middleVal = keepCardVals[i];
          }
        }

        //if the highest and lowest value or not in range add them to suitThrowAwayCards
        if(highestVal - lowestVal < hands[hand]["numOfCards"]){
          //add middleVal to the suitThrowAwayCards
          console.log("throw away middle val");
          console.log(keepCardVals);
          suitThrowAwayCards.push(keepCards[keepCardVals.indexOf(middleVal)]);
          highCardAction(suitThrowAwayCards);
        }
        else if(middleVal - lowestVal < hands[hand]["numOfCards"]){
          //add highestVal to the suitThrowAwayCards
          console.log("throw away highest val");
          console.log(keepCardVals);
          suitThrowAwayCards.push(keepCards[keepCardVals.indexOf(highestVal)]);
          highCardAction(suitThrowAwayCards);
        }
        else{
          console.log("throw away any card");
          console.log(keepCardVals);
          for(i = 0; i < keepCards.length; i++){
            suitThrowAwayCards.push(keepCards[i]);
            highCardAction(suitThrowAwayCards);
          }
        }

        console.log(suitThrowAwayCards);
        console.log("one throw away");
      }
      else{

        console.log("last");
        console.log("suits match");
        //make sure the values are in a workable run
        //check if keep cards are a run
        //sort array in order from highest to lowest
        if(Math.abs(keepCards[0]["numValue"] - keepCards[1]["numValue"]) > hands[hand]["numOfCards"]){
          suitThrowAwayCards.push(keepCards[0]);
          suitThrowAwayCards.push(keepCards[1]);
          highCardAction(suitThrowAwayCards);

        }
        else{
          highCardAction(suitThrowAwayCards);
        }
        console.log(keepCards);
        console.log(suitThrowAwayCards);
        /*highCardAction(suitThrowAwayCards);*/
      }
    }

    //if there are duplicate suits verify that they are in a workable range

    //if there are not duplicate values or suit or they are not in a workable range throw away the largest lose value card
  }
  else{
    console.log("Error: the value was not stack or deck");
  }

}


function checkStackAction(){
  var matches = 0;
  console.log(computerHand);
  for(i = 0; i < computerHand.length; i++){
    if(computerHand[i]["value"] == stack[0]["value"]){
      keepCards.push(computerHand[i]);
      keepCards.push(stack[0]);
      matches++;
    }
    else if(computerHand[i]["suit"] == stack[0]["suit"])
    {
      //make sure the value of the card with the same suit is in a usable range
      if(Math.abs(computerHand[i]["numValue"] - stack[0]["numValue"]) < hands[hand]["numOfCards"]){
        keepCards.push(computerHand[i]);
        keepCards.push(stack[0]);
        matches++;
      }
    }
    else{
      matches = matches;
    }
  }
  return matches;
}


function highCardAction(cards){
  console.log("test");
  var cardValues = [];
  var highestVal;
  for(i = 0; i < cards.length; i++)
  {
    cardValues.push(cards[i]["loseValue"]);
  }

  console.log(cardValues);
  highestVal = Math.max(...cardValues);
  console.log(highestVal);
  for(i = 0; i < cards.length; i++)
  {
    if(cards[i]["loseValue"] == highestVal){
      removeCardFromComputerHand(i);
      console.log(computerHand);
    }
  }


}

function startGame(){
  createDeck();
  shuffleDeck();
  saveBeforeDeck();
  startFirstHand();
}

startGame();
